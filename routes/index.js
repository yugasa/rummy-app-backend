var express = require('express');
var router = express.Router();
// var User = require('../models/user');
const db = require('../models/all-models');
var mongoose  = require('mongoose');
 const jwt = require('jsonwebtoken');
var  bcrypt  = require('bcryptjs');
const cors = require('cors');
router.use(cors());


// const Cashfree = require("cashfree-sdk");
// const PG = require('cashfree-sdk').PG;
// const Transactions = PG.Transactions;


// Initialize Cashfree Payment Gateway
// let Payouts = Cashfree.Payouts;
// Payouts.Init({
//     "ENV": "TEST", 
//     "ClientID": "CLIENTID",
//     "ClientSecret": "CLIENTSECRET"
// });
// var User      = mongoose.model('User');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/signup', async function(req, res, next) {
  const user = await db.User.findOne({phone:req.body.phone});
  if(user){
    return res.status(200).json({
      status:'failed',
      message:'This phone is already registered'
    });
  }
  req.body.password=bcrypt.hashSync(req.body.password);
  //new user data store in User collection.
  await db.User.create(req.body); 
  return res.status(200).json({
    status:'success',
    message:'User successfully registered!',
  });
});





router.post('/login', async function (req, res, next) {
  const user = await db.User.findOne({phone:req.body.phone});
  if(!user){
    return res.status(200).json({
      status:'failed',
      message:'you are not registered please signup!'
    });
  }
  //password verify.
  let passwordIsValid =await bcrypt.compareSync(req.body.password, user.password);
  if(!passwordIsValid){
    return res.status(200).json({
      status : 'failed',
      message : 'Password is incorrect.'
    });
   }
  //Generate a user_token.
  //let userToken = randomstring.generate(25);
  //Generate a jwt token.
  let authToken =await jwt.sign({userId:user._id},'rummyAppSecretKey');
  const updatedUser = await db.User.findOne({phone:req.body.phone},{password:0, _id:0}); 
  return res.status(200).json({
    status : 'success',
    message : 'Login successfully.',
    user:updatedUser,
    authToken:authToken
  });
});

// router.get('/profile', function (req, res, next) {
// 	console.log("profile");
// 	User.findOne({unique_id:req.session.userId},function(err,data){
// 		console.log("data");
// 		console.log(data);
// 		if(!data){
// 			res.redirect('/');
// 		}else{
// 			//console.log("found");
// 			return res.render('data.ejs', {"name":data.username,"email":data.email});
// 		}
// 	});
// });

// router.get('/logout', function (req, res, next) {
// 	console.log("logout")
// 	if (req.session) {
//     // delete session object
//     req.session.destroy(function (err) {
//     	if (err) {
//     		return next(err);
//     	} else {
//     		return res.redirect('/');
//     	}
//     });
// }
// });

// router.get('/forgetpass', function (req, res, next) {
// 	res.render("forget.ejs");
// });

// router.post('/forgetpass', function (req, res, next) {
// 	//console.log('req.body');
// 	//console.log(req.body);
// 	User.findOne({email:req.body.email},function(err,data){
// 		console.log(data);
// 		if(!data){
// 			res.send({"Success":"This Email Is not regestered!"});
// 		}else{
// 			// res.send({"Success":"Success!"});
// 			if (req.body.password==req.body.passwordConf) {
// 			data.password=req.body.password;
// 			data.passwordConf=req.body.passwordConf;

// 			data.save(function(err, Person){
// 				if(err)
// 					console.log(err);
// 				else
// 					console.log('Success');
// 					res.send({"Success":"Password changed!"});
// 			});
// 		}else{
// 			res.send({"Success":"Password does not matched! Both Password should be same."});
// 		}
// 		}
// 	});
	
// });
 router.get('/payment',function(req,res,next){
  res.render("index.ejs");	
 });

 router.post('/payment',function(req, res, next){
  	let player= new db.Player(req.body);

	 player.save(function(err){
     if(err){
      return handleError(err,res);
   }
   else{
     res.json(player);
   }
 });
  

});

function verifyToken(req,res,next){
	const bearerHeader = req.headers['authorization'];
	if(typeof bearerHeader !== 'undefined'){
    const bearer = bearerHeader.split('');
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next(); 
	}else{
		res.sendStatus(403);
	}
}



module.exports = router;
